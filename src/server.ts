import { app } from './app';
import { Router, Request, Response } from 'express';

const PORT = process.env.PORT || 5000;
const routes = Router();

routes.get('/studies', (Request: Request, Response: Response) =>
  Response.status(200).json({ message: 'Hello World...' })
);

app.use(routes);
app.listen(PORT, () => console.log(`Server listening to the port: ${PORT}`));
