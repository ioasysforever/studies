import express from 'express';
import compression from 'express';
import helmet from 'helmet';
import cors from 'cors';

const app = express();
app.use(express.json());

app.use(compression());
app.use(helmet());
app.use(cors());

export { app };
